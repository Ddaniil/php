<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>211-321_Иванцов_Даниил</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <header class='header'>
        <img class='header__logo' src="logo_polytech.png" alt="логотип">
        <h1 class='header__heading'>Настройка окружения</h1>
    </header>
    <main class='main'>
        <?php
            $names = ['Овен','Баран'];
            $dates = ['21.03-20.04','1991, 2003, 2015'];
            for ($i = 0; $i < count($names); $i++) {
                $card__number = $i+1;
                echo"
                    <section class='main__list'>
                        <div class='main__list__card'>
                            <h2 class='main__list__card__heading'>$names[$i]</h2>
                            <p class='main__list__card__text'>$dates[$i]</p>
                        </div>
                    </section>
                ";
            }
        ?>
    </main>
    <footer class='footer'>
        <p class='footer__text'>Иванцов Д. В.</p>
        <p class='footer__text'>211-321</p>
    </footer>
</body>
</html>